=== Pinterest Feed ===
Contributors: weblizar
Donate link: https://www.weblizar.com
Tags: pinterest, pinterest feed, pinterest wordpress plugin, pinterest profile, pinterest boards
Requires at least: 4.0
Tested up to: 4.9.2
Stable tag: 1.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Pinterest Feed is the best Pinterest album gallery plugin which allows you to showcase your Pinterest images on your website without any hassle. Responsive Pinterest image album makes sure it looks good on each and every device.

== Description ==

Pinterest feed is very easy to setup and useful plugin, has both shortcode and widget options. Make your content's social reach to next level.

Pinterest is a great booming social network that lets you organize and share all the beautiful things you find on the web, people use pins, boards to plan their events like weddings, decorate their homes, organize their favourite recipes, etc.

We made sure the Pinterest buttons, profile or board display integrate perfectly in any template widget. Bring new visits and users to your WordPress powered website by pin it's pages or by showing off your photos.

Pinterest Feed WordPress Plugin is used to show your Pinterest images in your website.

To display Pinterest feeds on your site use **[pinterest_feed]** shortcode in any Page or Post.

https://www.youtube.com/watch?v=DAuR30Gg7I4

### Features Of Plugin

* Pinterest Profile
Show your Pinterest profile as it looks on Pinterest platform

* Pinterest Pins
Share all your Pinterest pins on your website

* Get All relevant outputs of your Pinterest profile, Pins
Showcase your Pinterest gallery in the most efficient way to gather more traffic.

* Shortcode and widgets section for your Pinterest profile, Pins
Many shortcodes and widget options to choose where to display your Pinterest feed.

* Pinterest profile with stat box of total boards, Pins, Following, Followers, Likes and Follow button
Decide which stats you want to show to your users with beautiful templates.

* Get All Pinterest Pins of single and multiple boards
Choose how you want to display your pins and boards.

* Responsive dashboard design with Live preview
Tested with multiple devices and screen sizes by our team of dedicated web developers

* Easy to use user friendly interface
Easy options to configure Pinterest feed

* User friendly descriptive section designing with helpful tool tips
Stuck somewhere? Our special tool tips will help you along the way

* Multi Site Support
Plugin support on WordPress multi sites functionality

* Bootstrap Based Responsive Plugin Settings Panel
It is compatible with all the latest and top most WordPress websites available.

* Multilingual & Translation Ready
So that none of our user face difficulty in using our plugin.

#### Check Pinterest Feed Pro Live In Action

* [Pinterest Feed](http://demo.weblizar.com/pinterest-feed-pro/)
* [Pinterest Profile](http://demo.weblizar.com/pinterest-feed-pro/profile/)
* [Pinterest Pins](http://demo.weblizar.com/pinterest-feed-pro/pinterest-pins/)
* [Pinterest Boards](http://demo.weblizar.com/pinterest-feed-pro/pinterest-board/)
* [Pinterest Feed Pro Docs](https://weblizar.com/documentation/plugins/pinterest-feed-pro/)

#### Check Admin Demo Before Buy
* Login URL: [http://demo.weblizar.com/pinterest-feed-pro-admin-demo/wp-admin/](http://demo.weblizar.com/pinterest-feed-pro-admin-demo/wp-admin/)
* Username: userdemo
* Password: userdemo

### Translators

Please contribute to translate our plugin. Contact at `Lizarweb (at) Gmail (dot) Com`.

Our test team tested this plugin many times on different servers. You as our potential tester will do the final testing. So, your feedback is really important for us. You can ask support question on WordPress forum support of this plugin and our experienced support team will resolve each & every issue you are facing. So, do not hesitate to download and install this plugin.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the `weblizar-pinterest-feeds` folder to the to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Then Go to pluign menu page
4. Configure plugin settings and use **pinterest_feed]** shortcode on any Page / Post to show your Pinterest profile feeds
5. You can also show Pinterest profile feeds using widgets, which is avalaible into Admin Menu --> Appearance --> Widgets
6. Use it & Enjoy.

== Frequently Asked Questions ==

Please use WordPress support forum to ask any query regarding any issue.

== Screenshots ==

1. Live Preview of Pinterest Profile & Pins	
2. Pinterest Profile With Selected Boards
3. Pinterest Profile & Pins
4. Pinterest Profile & Pins Widget into Sidebar
5. Pinterest Profile
6. Pinterest Access Token Settings
7. Pinterest Profile Settings
8. Pinterest Pins Settings
9. Pinterest Widget Settings

== Changelog ==

= 1.1.3 - Expired Access Token updated and some of file code intended and modified
= 1.1.2 - All admin data settings sanitized and added ajax nonce to prevent external data posting and hack
= 1.1.1 - Pins Template setting on/off not working properly bug fixed + version update and plugin compatible to wp 4.9.1
= 1.1.0 - version update and plugin compatible to wp 4.9.1
= 1.0.9 - version update and plugin compatible to wp 4.9
= 1.0.8 - version update
= 1.0.7 - Pinterest official API change updated into plugin like pins now save
= 1.0.6 - version update and plugin compatible to wp 4.8.1
= 1.0.5 - version update and plugin compatible to wp 4.8.1
= 1.0.4 - version update and plugin compatible to wp 4.8
= 1.0.3 - version update and plugin compatible to wp 4.7.5
= 1.0.2 - version update and plugin compatible to wp 4.7.4
= 1.0.1 - version update and plugin compatible to wp 4.7.3
= 1.0.0 =
* Initial release.