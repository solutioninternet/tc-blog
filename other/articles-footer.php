<?php
define('WP_USE_THEMES', false);
require('/home/TC_blog/wp-blog-header.php');

$num = $_GET["numberposts"] ?: '4';
$cat = $_GET["catid"];
$postID = $_GET["postid"];
$type = $_GET["type"];

switch ($type){
   case "singlePost":
      $posts = get_posts('p='.$postID.'&numberposts=1');
      break;
   case "catFeed":
      $posts = get_posts('category='.$cat.'&numberposts='.$num.'&order=ASC');
      break;
   default:
      $posts = get_posts('numberposts='.$num.'&order=DESC');
}
?>

<style>
.bloc-article-wp-img:hover, .bloc-article-wp-txt:hover { opacity: 0.75 !important; }
</style>

<center>
<?php foreach ($posts as $post) : start_wp(); ?>
<div class="bloc-article-wp" style="width:25%; float:left;">
<a href="<?php the_permalink_rss() ?>" target="_parent" atl="<?php the_title();?>" title="<?php the_title();?>" style="text-decoration:none;">
<div class="bloc-article-wp-img" style="max-width:90%;width:285px;height:155px;background:url('<?php the_post_thumbnail_url(); ?>'); margin-bottom:10px; background-size: cover; background-position: 50% 50%;">
</div></a>
<a class="bloc-article-wp-txt" atl="<?php the_title();?>" href="<?php the_permalink_rss() ?>" target="_parent" style="text-decoration:none; margin-top: 10px; color:black; text-decoration:none; font-family: arial,'Trebuchet MS', 'Helvetica Neue', Helvetica, sans-serif; font-size:14px;"><?php the_title();?></div>
</div>
</a>
<?php endforeach; ?>
</center>

