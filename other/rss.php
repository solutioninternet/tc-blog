<?php
define('WP_USE_THEMES', false);
require('/home/TC_blog/wp-blog-header.php');

$num = $_GET["numberposts"] ?: '4';
$cat = $_GET["catid"];
$postID = $_GET["postid"];
$type = $_GET["type"];

switch ($type){
   case "singlePost":
      $posts = get_posts('p='.$postID.'&numberposts=1');
      break;
   case "catFeed":
      $posts = get_posts('category='.$cat.'&numberposts='.$num.'&order=ASC');
      break;
   default:
      $posts = get_posts('numberposts='.$num.'&order=DESC');
}
?>

<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Conseils pour Bien Construire sa Maison</title>
	<atom:link href="http://www.terrain-construction.com/content/feed/" rel="self" type="application/rss+xml" />
	<link>http://www.terrain-construction.com/content</link>
	<description></description>
	<lastBuildDate>Mon, 22 May 2017 09:34:18 +0000</lastBuildDate>
	<language>fr-FR</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	<generator>https://wordpress.org/?v=4.7.5</generator>
	

<?php foreach ($posts as $post) : start_wp(); ?>
<item>
		<title><img src="<?php the_post_thumbnail_url(); ?>" /><br/><?php the_title();?></title>
		<link><?php the_permalink_rss() ?></link>
</item>
<?php endforeach; ?>

	</channel>
</rss>
