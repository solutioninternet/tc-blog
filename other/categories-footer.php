<?php
define('WP_USE_THEMES', false);
require('/home/TC_blog/wp-blog-header.php');
?>


<style>
.bloc-article-wp-img:hover, .bloc-article-wp-txt:hover { opacity: 0.75 !important; }
</style>

<center>

<?php 

get_the_categories();

function get_the_categories( $parent = 0 ) 
{
    $categories = get_categories( "hide_empty=1&parent=$parent" );

    if ( $categories ) {
        $i = 0;
        foreach ( $categories as $cate ) {
            if ( $cat->category_parent == $parent ) {

                get_the_categories( $cate->term_id );
                $post_cate = get_posts(array('numberposts' => 1,'category' => $cate->term_id));

                foreach ($post_cate as $post_cata) : start_wp();

                    if($i < 4) {
                        $url_imge = get_the_post_thumbnail_url($post_cata,'full');
                        echo '<div class="bloc-article-wp" style="width:25%; float:left;"> <a href="' . get_category_link( $cate->term_id ) . '" atl="' . $cate->name . '" title="' . $cate->name . '" target="_parent" style="text-decoration:none;">';
                        echo '<div class="bloc-article-wp-img" style="max-width:90%;width:285px;height:155px;background:url(' . $url_imge . '); margin-bottom:10px; background-size: cover; background-position: 50% 50%;"> </div></a>';
                        echo '<a class="bloc-article-wp-txt" atl="' . $cate->name . '" href="' . get_category_link( $cate->term_id ) . '" target="_parent" style="text-decoration:none; margin-top: 10px; color:black; text-decoration:none; font-family: arial,Trebuchet, Helvetica, sans-serif; font-size:14px;">' . $cate->name . '</div></div></a>';
                    }

                    $i++;

                endforeach;

            }
        }
        
    }
}

?>

</center>


