<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?K=wx(cGB2q6co8s6g!XHYQTPe%=YF[ClW.#hQq:e.KQA d]^{5W]b%ml~/75kN5');
define('SECURE_AUTH_KEY',  'Mi3*}tfI?aUmm~064(?N(>T1N0T9B((-l{,rq.qV5)xW~,Q{YzTi@@9N&-z?$XQ%');
define('LOGGED_IN_KEY',    'VGW$wfF4)*o|krC%k2vScIN0f6m eD)mQtID!},.u3A]/WDr!=[8g[]%~- w5`fx');
define('NONCE_KEY',        'b?8oU=a?d6I$tus%fs]4r|pl{5en::K *A+?6Nz5C|Fmxsf|gj|aAeL1#U/xmu0a');
define('AUTH_SALT',        'Wk7Y(MN0>6aY*OL[h%fw:En8r<Aq)Hul*>L+xT2|<XT$vEr(}JB|Rw4yqmgQ;%>p');
define('SECURE_AUTH_SALT', 'eJ.Y|UJ(:0Y~q(S*=s=wz|dp:%Bue|VRtif2+`IS_jyo2`ON#K:L//T31Zlj;^-/');
define('LOGGED_IN_SALT',   '@L3%4Y4}TIh|$f^TG#|5GQgz9)3$y?3N<,`EtiG<^4e7IR=uxhK9$IhallC1UEKS');
define('NONCE_SALT',       'mO]8ce&2g<3?eC8H3FbvJVi;hh_4c{0XLrQE*FcyJ,{[`FC:F3L&36NWH:!xSONG');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');
/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '512M');


